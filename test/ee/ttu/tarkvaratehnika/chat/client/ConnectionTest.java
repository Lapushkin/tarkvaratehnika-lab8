package ee.ttu.tarkvaratehnika.chat.client;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.RunnableWithExceptions;
import ee.ttu.tarkvaratehnika.chat.TestUtil;
import ee.ttu.tarkvaratehnika.chat.server.ClientConnection;
import ee.ttu.tarkvaratehnika.chat.server.MessageProcessor;
import ee.ttu.tarkvaratehnika.chat.server.Server;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ConnectionTest {
	private Server server;

	@After
	public void stopServer() {
		server.stop();
	}

	@Before
	public void setUp() throws InterruptedException {
		Thread.sleep(1000);
	}

	@Test
	public void connectToServer() throws Exception {
		final AtomicBoolean clientConnected = new AtomicBoolean(false);
		server = new Server(new MessageProcessor() {
			@Override
			public void clientConnected(ClientConnection client) {
				clientConnected.set(true);
			}
		});
		new Thread(server).start();

		new Connection(new NullMessageReceiver());

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() throws Exception {
				assertTrue(clientConnected.get());
			}
		});
	}

	@Test
	public void sendMessageToServer() throws Exception {
		final AtomicReference<Message> receivedOnServer = new AtomicReference<Message>(
				null);
		server = new Server(new MessageProcessor() {
			@Override
			protected void messageReceivedFrom(ClientConnection client,
					Message message) {
				receivedOnServer.set(message);
			}
		});
		new Thread(server).start();

		new Connection(new NullMessageReceiver()).sendToServer(new Message(
				"Sent from client"));

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() throws Exception {
				assertEquals("Sent from client", receivedOnServer.get()
						.getText());
			}
		});
	}

	@Test
	public void receiveMessageFromServer() throws Exception {
		server = new Server(new MessageProcessor() {
			@Override
			public void clientConnected(ClientConnection client) {
				client.send(new Message("Message from server"));
			}
		});
		new Thread(server).start();

		final AtomicReference<Message> receivedOnClient = new AtomicReference<Message>(
				null);
		new Connection(new NullMessageReceiver() {
			@Override
			public void messageFromServer(MessageSender connection,
					Message message) {
				receivedOnClient.set(message);
			}
		});

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() throws Exception {
				assertEquals("Message from server", receivedOnClient.get()
						.getText());
			}
		});
	}

	// TODO: This method too long
	@Test
	public void sendAndReceiveMultipleMessagesWithServer() throws Exception {
		server = new Server(new MessageProcessor() {
			@Override
			protected void messageReceivedFrom(ClientConnection client,
					Message message) {
				if ("Message 1 from client".equals(message.getText())) {
					client.send(new Message("Server responds to message 1"));
				} else if ("Message 2 from client".equals(message.getText())) {
					client.send(new Message("Server responds to message 2"));
				} else {
					throw new RuntimeException("Unknown message");
				}
			}
		});
		new Thread(server).start();

		final AtomicBoolean flowCompleted = new AtomicBoolean(false);
		new Connection(new NullMessageReceiver() {
			@Override
			public void connectedToServer(MessageSender connection) {
				connection.sendToServer(new Message("Message 1 from client"));
			}

			@Override
			public void messageFromServer(MessageSender connection,
					Message message) {
				if ("Server responds to message 1".equals(message.getText())) {
					connection
							.sendToServer(new Message("Message 2 from client"));
				} else if ("Server responds to message 2".equals(message
						.getText())) {
					flowCompleted.set(true);
				} else {
					throw new RuntimeException("Unknown message");
				}
			}
		});

		waitUntilTrue(flowCompleted);
	}

	private void waitUntilTrue(final AtomicBoolean flag) {
		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() throws Exception {
				assertTrue(flag.get());
			}
		});
	}
}


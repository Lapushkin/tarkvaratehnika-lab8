package ee.ttu.tarkvaratehnika.chat.client;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class NullMessageReceiver implements MessageReceiver {
	public void messageFromServer(MessageSender connection, Message message) {
	}

	public void connectedToServer(MessageSender connection) {
	}

	public void sendEmoticonButtonClicked() {
	}
}

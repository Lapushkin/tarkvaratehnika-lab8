package ee.ttu.tarkvaratehnika.chat.client.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MouseInputListener;
import javax.swing.text.Document;
import javax.swing.text.Element;

import org.junit.Before;
import org.junit.Test;

public class ChatWindowTest {

	private final class MockDocumentEvent implements DocumentEvent {
		@Override
		public EventType getType() {
			return null;
		}

		@Override
		public int getOffset() {
			return 0;
		}

		@Override
		public int getLength() {
			return 0;
		}

		@Override
		public Document getDocument() {
			return null;
		}

		@Override
		public ElementChange getChange(Element elem) {
			return null;
		}
	}

	private ChatWindow chatWindow;

	@Before
	public void setUp() {
		chatWindow = new ChatWindow();
	}

	@Test
	public void testWindowLayout() {
		assertEquals(600, chatWindow.getSize().getHeight(), 0);
		assertEquals(800, chatWindow.getSize().getWidth(), 0);
		assertEquals(100, chatWindow.getLocation().getX(), 0);
		assertEquals(100, chatWindow.getLocation().getY(), 0);

		assertEquals(WindowConstants.DISPOSE_ON_CLOSE,
				chatWindow.getDefaultCloseOperation());
	}

	@Test
	public void testWindowElements() {
		assertEquals(BorderLayout.class, chatWindow.getLayout().getClass());

		assertEquals(2, chatWindow.getContentPane().getComponentCount());
		assertEquals(JScrollPane.class, chatWindow.getContentPane()
				.getComponent(0).getClass());
		assertTrue(((JScrollPane) chatWindow.getContentPane().getComponent(0))
				.getAutoscrolls());
		assertEquals(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				((JScrollPane) chatWindow.getContentPane().getComponent(0))
						.getVerticalScrollBarPolicy());
		assertEquals(JPanel.class, chatWindow.getContentPane().getComponent(1)
				.getClass());

		assertNotNull(chatWindow.presenter);
	}

	@Test
	public void testChartContents() {
		assertNotNull(chatWindow.chatContents);
		assertFalse(chatWindow.chatContents.isEditable());
		assertTrue(chatWindow.chatContents.getLineWrap());
	}

	@Test
	public void testEmoticonButton() {
		assertNotNull(chatWindow.emoticonButton);
		
		MockChatWindowPresenter mockPresenter = new MockChatWindowPresenter(
				chatWindow);
		chatWindow.presenter = mockPresenter;
		ActionListener[] actionListeners = chatWindow.emoticonButton
				.getListeners(ActionListener.class);
		assertEquals(1, actionListeners.length);
		actionListeners[0]
				.actionPerformed(new ActionEvent(new Object(), 1, ""));
		assertTrue("presenter not called",
				mockPresenter.emoticonButtonClicked.get());
	}

	@Test
	public void testTextEntryFieldActionListeners() {
		MockChatWindowPresenter mockPresenter = new MockChatWindowPresenter(
				chatWindow);
		chatWindow.presenter = mockPresenter;
		ActionListener[] actionListeners = chatWindow.textEntryField
				.getListeners(ActionListener.class);
		assertEquals(1, actionListeners.length);
		actionListeners[0]
				.actionPerformed(new ActionEvent(new Object(), 1, ""));
		assertTrue("presenter not called",
				mockPresenter.sendButtonClicked.get());
	}

	@Test
	public void testTextEntryFieldDocumentListeners() {
		MockChatWindowPresenter mockPresenter = new MockChatWindowPresenter(
				chatWindow);
		chatWindow.presenter = mockPresenter;

		DocumentListener[] documentListeners = ((javax.swing.text.AbstractDocument) (chatWindow.textEntryField
				.getDocument())).getDocumentListeners();
		assertEquals(3, documentListeners.length);

		documentListeners[0].changedUpdate(new MockDocumentEvent());
		assertTrue("presenter not called", mockPresenter.enterTextChanged.get());
		mockPresenter.enterTextChanged.set(false);
		documentListeners[0].removeUpdate(new MockDocumentEvent());
		assertTrue("presenter not called", mockPresenter.enterTextChanged.get());
		mockPresenter.enterTextChanged.set(false);
		documentListeners[0].insertUpdate(new MockDocumentEvent());
		assertTrue("presenter not called", mockPresenter.enterTextChanged.get());
	}

	@Test
	public void testSendbuttonActionListeners() {
		MockChatWindowPresenter mockPresenter = new MockChatWindowPresenter(
				chatWindow);
		chatWindow.presenter = mockPresenter;
		ActionListener[] actionListeners = chatWindow.sendButton
				.getListeners(ActionListener.class);
		assertEquals(1, actionListeners.length);
		actionListeners[0]
				.actionPerformed(new ActionEvent(new Object(), 1, ""));
		assertTrue("presenter not called",
				mockPresenter.sendButtonClicked.get());
	}

	@Test
	public void testSetChatAreaContents() {
		chatWindow.setChatAreaContents("blabla");
		assertEquals("blabla", chatWindow.chatContents.getText());
		assertEquals("blabla".length(),
				chatWindow.chatContents.getCaretPosition());
	}

	@Test
	public void testSetEnteredText() {
		chatWindow.setEnteredText("test tekst");
		assertEquals("test tekst", chatWindow.textEntryField.getText());
	}

	@Test
	public void testEmoticonPopupInit() {
		assertNotNull(chatWindow.emoticonWindow);
		assertEquals(150,chatWindow.emoticonWindow.getLocation().x,0);
		assertEquals(150,chatWindow.emoticonWindow.getLocation().y,0);
		assertEquals(150,chatWindow.emoticonWindow.getSize().height,0);
		assertEquals(50,chatWindow.emoticonWindow.getSize().width,0);
	}

	@Test
	public void testEmoticonInsert(){
		chatWindow.emoticonWindow.getEmoticonsTable().getMouseListeners()[2].mouseClicked(null);
		assertEquals(chatWindow.textEntryField.getText(), ":)");
	}
}

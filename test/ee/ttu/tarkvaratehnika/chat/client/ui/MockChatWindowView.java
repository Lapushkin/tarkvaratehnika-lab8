package ee.ttu.tarkvaratehnika.chat.client.ui;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class MockChatWindowView implements ChatWindowView {
	public final AtomicReference<String> enteredText = new AtomicReference<String>("");
	public final AtomicReference<String> chatAreaText = new AtomicReference<String>("");
	public final AtomicBoolean sendButtonEnabled = new AtomicBoolean(false);
	public final AtomicBoolean emoticonWindowVisible = new AtomicBoolean(false);

	@Override
	public void setChatAreaContents(String text) {
		chatAreaText.set(text);
	}

	@Override
	public String getEnteredText() {
		return enteredText.get();
	}

	@Override
	public void setEnteredText(String text) {
		enteredText.set(text);
	}

	@Override
	public void setSendButtonEnabled(boolean enabled) {
		sendButtonEnabled.set(enabled);
	}

	@Override
	public void showEmoticonPopup() {
		emoticonWindowVisible.set(true);
	}
}

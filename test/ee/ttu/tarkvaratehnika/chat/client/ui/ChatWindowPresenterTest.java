package ee.ttu.tarkvaratehnika.chat.client.ui;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.atomic.AtomicReference;

import org.junit.Before;
import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.client.MessageSender;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ChatWindowPresenterTest {
	private MockMessageSender mockMessageSender = new MockMessageSender();
	private MockChatWindowView mockView = new MockChatWindowView();

	private ChatWindowPresenter presenter = new ChatWindowPresenter(mockView);

	@Before
	public void setUpPresenter() {
		presenter.connectedToServer(mockMessageSender);
	}

	@Test
	public void remembersAllChatMessagesAndDisplaysThemOnTheView()
			throws Exception {
		presenter.messageFromServer(null, new Message("Message 1"));
		presenter.messageFromServer(null, new Message("Message 2"));

		assertEquals("Message 1\nMessage 2\n", mockView.chatAreaText.get());
	}

	@Test
	public void clearsEnteredTextWhenSendButtonIsClicked() throws Exception {
		presenter.sendButtonClicked();

		assertEquals("", mockView.enteredText.get());
	}

	@Test
	public void sendsEnteredTextToServerWhenSendButtonIsClicked()
			throws Exception {
		mockView.setEnteredText("Message to send");
		presenter.sendButtonClicked();

		assertEquals("Message to send", mockMessageSender.sentMessage.get()
				.getText());
	}

	@Test
	public void doesNotSendEmptyTextToServer() throws Exception {
		mockView.setEnteredText("");
		presenter.sendButtonClicked();

		assertNull(mockMessageSender.sentMessage.get());
	}

	@Test
	public void enablesSendButtonWhenThereIsTextEntered() throws Exception {
		mockView.setEnteredText("Some text");
		presenter.enteredTextChanged();

		assertTrue(mockView.sendButtonEnabled.get());
	}

	@Test
	public void disablesSendButtonWhenThereIsNoTextEntered() throws Exception {
		mockView.setEnteredText("");
		presenter.enteredTextChanged();

		assertFalse(mockView.sendButtonEnabled.get());
	}

	@Test
	public void disablesSendButtonWhenThereIsOnlySpacesEntered()
			throws Exception {
		mockView.setEnteredText("    ");
		presenter.enteredTextChanged();

		assertFalse(mockView.sendButtonEnabled.get());
	}

	@Test
	public void openEmoticonPopup() throws Exception {
		presenter.sendEmoticonButtonClicked();

		assertTrue("popup display not shown", mockView.emoticonWindowVisible.get());
	}

	public static class MockMessageSender implements MessageSender {
		public final AtomicReference<Message> sentMessage = new AtomicReference<Message>(
				null);

		@Override
		public void sendToServer(Message message) {
			sentMessage.set(message);
		}
	}
}

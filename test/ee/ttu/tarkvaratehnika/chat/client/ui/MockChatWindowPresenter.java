package ee.ttu.tarkvaratehnika.chat.client.ui;

import java.util.concurrent.atomic.AtomicBoolean;

public class MockChatWindowPresenter extends ChatWindowPresenter {

	public final AtomicBoolean sendButtonClicked = new AtomicBoolean(false);
	public final AtomicBoolean enterTextChanged = new AtomicBoolean(false);
	public final AtomicBoolean emoticonButtonClicked = new AtomicBoolean(false);

	public MockChatWindowPresenter(ChatWindowView view) {
		super(view);
	}

	@Override
	public void sendButtonClicked() {
		sendButtonClicked.set(true);
	}

	@Override
	public void enteredTextChanged() {
		if (enterTextChanged != null) {
			enterTextChanged.set(true);
		}
	}
	
	@Override
	public void sendEmoticonButtonClicked() {
		emoticonButtonClicked.set(true);
	}
}

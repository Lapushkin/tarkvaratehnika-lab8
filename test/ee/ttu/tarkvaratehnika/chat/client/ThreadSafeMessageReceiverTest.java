package ee.ttu.tarkvaratehnika.chat.client;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.RunnableWithExceptions;
import ee.ttu.tarkvaratehnika.chat.TestUtil;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ThreadSafeMessageReceiverTest {

	ThreadSafeMessageReceiver threadSafeMessageReceiver;
	MessageSender messageSender;

	@Before
	public void setUp() throws Exception {
		this.threadSafeMessageReceiver = new ThreadSafeMessageReceiver(
				new MessageReceiver() {

					@Override
					public void messageFromServer(MessageSender connection,
							Message message) {
						connection.sendToServer(message);
					}

					@Override
					public void connectedToServer(MessageSender connection) {
						connection.sendToServer(new Message("TestingSend"));
					}

					@Override
					public void sendEmoticonButtonClicked() {
					}
				});
	}

	@Test
	public void testConnectedToServer() throws InterruptedException {
		messageSender = new MessageSender() {
			private boolean executed = false;

			@Override
			public void sendToServer(Message message) {
				assertEquals("TestingSend", message.getText());
				executed = true;
			}

			@Override
			public boolean equals(Object arg0) {
				return executed;
			}

		};

		threadSafeMessageReceiver.connectedToServer(messageSender);
		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertTrue("sendToServer was not executed",
						messageSender.equals(null));
			}
		});
	}

	@Test
	public void testMessageFromServer() throws InterruptedException {
		messageSender = new MessageSender() {

			private boolean executed = false;

			@Override
			public void sendToServer(Message message) {
				assertEquals("MessageFromServer", message.getText());
				executed = true;
			}

			@Override
			public boolean equals(Object arg0) {
				return executed;
			}

		};
		threadSafeMessageReceiver.messageFromServer(messageSender, new Message(
				"MessageFromServer"));
		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertTrue("messageFromServer was not executed",
						messageSender.equals(null));
			}
		});
	}

}

package ee.ttu.tarkvaratehnika.chat;

public class TestUtil {
	private static final int WAIT_TIMEOUT = 1000;
	
	public static void waitForSuccess(RunnableWithExceptions runnable) {
		long startTime = System.currentTimeMillis();
		Throwable lastFailure = null;
		do {
			lastFailure = tryRunning(runnable);
			if(lastFailure == null) {
				return;
			}
		} while (timePassedAfter(startTime) < WAIT_TIMEOUT);
		
		throw new RuntimeException(lastFailure);
	}

	private static Throwable tryRunning(RunnableWithExceptions runnable) {
		try {
			runnable.run();
			return null;
		} catch(Throwable t) {
			return t;
		}
	}
	
	private static long timePassedAfter(long startTime) {
		return System.currentTimeMillis() - startTime;
	}
}

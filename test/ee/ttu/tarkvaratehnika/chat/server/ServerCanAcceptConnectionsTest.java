package ee.ttu.tarkvaratehnika.chat.server;

import static org.junit.Assert.assertEquals;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.RunnableWithExceptions;
import ee.ttu.tarkvaratehnika.chat.TestUtil;

public class ServerCanAcceptConnectionsTest {
	private MockServer mockServer = new MockServer();

	@After
	public void stopServer() {
		mockServer.stop();
	}

	@Test
	public void oneConnection() throws Exception {
		new Thread(mockServer).start();

		new Socket("localhost", Server.DEFAULT_SERVER_PORT);

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertEquals(1, mockServer.getConnectionsAccepted().size());
			}
		});
	}

	@Test
	public void twoConnections() throws Exception {
		new Thread(mockServer).start();

		new Socket("localhost", Server.DEFAULT_SERVER_PORT);
		new Socket("localhost", Server.DEFAULT_SERVER_PORT);

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertEquals(2, mockServer.getConnectionsAccepted().size());
			}
		});
	}

	public static class MockServer extends Server {
		public MockServer() {
			super(null);
		}

		protected final List<Socket> connectionsAccepted = new ArrayList<Socket>();

		protected void acceptNewClient(Socket connection) {
			synchronized (connectionsAccepted) {
				connectionsAccepted.add(connection);
			}
		}

		public List<Socket> getConnectionsAccepted() {
			synchronized (connectionsAccepted) {
				return new ArrayList<Socket>(connectionsAccepted);
			}
		}
	}
}

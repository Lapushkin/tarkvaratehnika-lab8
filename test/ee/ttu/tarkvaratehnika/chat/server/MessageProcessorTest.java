package ee.ttu.tarkvaratehnika.chat.server;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class MessageProcessorTest {
	private MessageProcessor mp = new MessageProcessor();

	private MockClientConnection client1 = new MockClientConnection();
	private MockClientConnection client2 = new MockClientConnection();

	@Test
	public void broadcastsMessagesToAllUsers() throws Exception {
		mp.clientConnected(client1);
		mp.clientConnected(client2);

		mp.messageReceivedFrom(client1, new Message("Hello everyone!"));

		assertEquals("user1: Hello everyone!",
				client1.getReceivedMessageTexts());
		assertEquals("user1: Hello everyone!",
				client2.getReceivedMessageTexts());
	}

	@Test
	public void doesNotBroadcastsToDisconnectedUsers() throws Exception {
		mp.clientConnected(client1);

		mp.clientConnected(client2);
		mp.clientDisconnected(client2);

		mp.messageReceivedFrom(client1, new Message("Hello everyone!"));

		assertEquals("user1: Hello everyone!",
				client1.getReceivedMessageTexts());
		assertEquals("", client2.getReceivedMessageTexts());
	}

	@Test
	public void givesListOfUsers() throws Exception {
		mp.clientConnected(client1);
		mp.clientConnected(client2);

		mp.messageReceivedFrom(client1, new Message("/listusers"));

		assertEquals("/listusers, user1, user2",
				client1.getReceivedMessageTexts());
		assertEquals("", client2.getReceivedMessageTexts());
	}

	private final class MockClientConnection extends ClientConnection {
		public final List<Message> receivedMessages = new ArrayList<Message>();

		private MockClientConnection() {
			super(null, null, null);
		}

		@Override
		public void send(Message message) {
			receivedMessages.add(message);
		}

		public String getReceivedMessageTexts() {
			String result = "";
			for (int i = 0; i < receivedMessages.size(); i++) {
				if (i > 0) {
					result += ", ";
				}
				result += receivedMessages.get(i).getText();
			}
			return result;
		}
	}
}

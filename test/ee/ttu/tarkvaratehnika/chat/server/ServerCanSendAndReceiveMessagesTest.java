package ee.ttu.tarkvaratehnika.chat.server;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ee.ttu.tarkvaratehnika.chat.RunnableWithExceptions;
import ee.ttu.tarkvaratehnika.chat.TestUtil;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ServerCanSendAndReceiveMessagesTest {
	private final MockMessageProcessor mockMessageProcessor = new MockMessageProcessor();
	private Server server;

	@After
	public void stopServer() {
		server.stop();
	}

	@Before
	public void setUp() throws InterruptedException {
		Thread.sleep(1000);
	}

	@Test
	public void messageProcessorGetsNotifiedWhenClientConnectsAndDisconnects()
			throws Exception {
		server = new Server(mockMessageProcessor);

		new Thread(server).start();

		SimpleClient client = new SimpleClient();

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertNotNull(mockMessageProcessor.connectedClient.get());
			}
		});

		client.close();

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertNotNull(mockMessageProcessor.disconnectedClient.get());
			}
		});
	}

	@Test
	public void receivingFromClient() throws Exception {
		server = new Server(mockMessageProcessor);

		new Thread(server).start();

		SimpleClient client = new SimpleClient();
		client.out.writeObject(new Message("text from client"));

		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() {
				assertEquals("text from client",
						mockMessageProcessor.receivedMessage.get().getText());
			}
		});
	}

	@Test
	public void sendingToClient() throws Exception {
		server = new Server(new MessageProcessor() {
			@Override
			public void clientConnected(ClientConnection client) {
				client.send(new Message("text from server"));
			}
		});

		new Thread(server).start();

		waitForMessage("text from server", new SimpleClient().in);
	}

	@Test
	public void sendingMultipleMessagesBackAndForth() throws Exception {
		server = new Server(new MessageProcessor() {
			@Override
			protected void messageReceivedFrom(ClientConnection client,
					Message message) {
				if ("Message 1 from client".equals(message.getText())) {
					client.send(new Message("Server responds to message 1"));
				} else if ("Message 2 from client".equals(message.getText())) {
					client.send(new Message("Server responds to message 2"));
				} else {
					throw new RuntimeException("Unknown message");
				}
			}
		});

		new Thread(server).start();

		SimpleClient client = new SimpleClient();
		client.out.writeObject(new Message("Message 1 from client"));
		waitForMessage("Server responds to message 1", client.in);

		client.out.writeObject(new Message("Message 2 from client"));
		waitForMessage("Server responds to message 2", client.in);
	}

	@Test
	public void sendingMessagesToMultipleClients() throws Exception {
		server = new Server(new MessageProcessor() {
			@Override
			protected void messageReceivedFrom(ClientConnection client,
					Message message) {
				if ("Message from client 1".equals(message.getText())) {
					client.send(new Message("Server responds to client 1"));
				} else if ("Message from client 2".equals(message.getText())) {
					client.send(new Message("Server responds to client 2"));
				} else {
					throw new RuntimeException("Unknown message");
				}
			}
		});

		new Thread(server).start();

		SimpleClient client1 = new SimpleClient();
		SimpleClient client2 = new SimpleClient();

		client1.out.writeObject(new Message("Message from client 1"));
		waitForMessage("Server responds to client 1", client1.in);

		client2.out.writeObject(new Message("Message from client 2"));
		waitForMessage("Server responds to client 2", client2.in);
	}

	private void waitForMessage(final String messageText,
			final ObjectInputStream in) {
		final StringBuffer completeReceivedText = new StringBuffer();
		TestUtil.waitForSuccess(new RunnableWithExceptions() {
			@Override
			public void run() throws Exception {
				completeReceivedText.append(((Message) in.readObject())
						.getText());
				assertEquals(messageText, completeReceivedText.toString());
			}
		});
	}

	public static class SimpleClient {
		public Socket socket;
		public ObjectInputStream in;
		public ObjectOutputStream out;

		public SimpleClient() throws IOException {
			socket = new Socket("localhost", Server.DEFAULT_SERVER_PORT);
			in = new ObjectInputStream(socket.getInputStream());
			out = new ObjectOutputStream(socket.getOutputStream());
		}

		public void close() throws IOException {
			socket.close();
		}
	}

	private static class MockMessageProcessor extends MessageProcessor {
		public final AtomicReference<Message> receivedMessage = new AtomicReference<Message>(
				null);
		public final AtomicReference<ClientConnection> disconnectedClient = new AtomicReference<ClientConnection>(
				null);
		public final AtomicReference<ClientConnection> connectedClient = new AtomicReference<ClientConnection>(
				null);

		@Override
		protected void messageReceivedFrom(ClientConnection client,
				Message message) {
			receivedMessage.set(message);
		}

		@Override
		public void clientConnected(ClientConnection client) {
			connectedClient.set(client);
		}

		@Override
		public void clientDisconnected(ClientConnection client) {
			disconnectedClient.set(client);
		}

	}
}

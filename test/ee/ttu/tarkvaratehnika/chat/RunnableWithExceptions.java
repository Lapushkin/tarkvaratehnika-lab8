package ee.ttu.tarkvaratehnika.chat;

public interface RunnableWithExceptions {
    public void run() throws Exception;
}

package ee.ttu.tarkvaratehnika.chat.client;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public interface MessageSender {

	void sendToServer(Message message);

}
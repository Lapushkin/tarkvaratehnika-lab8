package ee.ttu.tarkvaratehnika.chat.client.ui;

public interface ChatWindowView {

	void setChatAreaContents(String text);

	String getEnteredText();

	void setEnteredText(String text);

	void setSendButtonEnabled(boolean b);

	void showEmoticonPopup();

}

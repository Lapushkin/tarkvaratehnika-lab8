package ee.ttu.tarkvaratehnika.chat.client.ui;

import ee.ttu.tarkvaratehnika.chat.client.MessageReceiver;
import ee.ttu.tarkvaratehnika.chat.client.MessageSender;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ChatWindowPresenter implements MessageReceiver {
	private final ChatWindowView view;
	
	private MessageSender connection; 
	private String chatContents = "";

	public ChatWindowPresenter(ChatWindowView view) {
		this.view = view;
		
		enteredTextChanged();
	}

	@Override
	public void messageFromServer(MessageSender connection, Message message) {
		chatContents += message.getText() + "\n";
		view.setChatAreaContents(chatContents);
	}

	@Override
	public void connectedToServer(MessageSender connection) {
		this.connection = connection;
	}

	public void sendButtonClicked() {
		if (isTextAreaFilled())
			connection.sendToServer(new Message(view.getEnteredText()));
		view.setEnteredText("");
	}

	public void enteredTextChanged() {
		view.setSendButtonEnabled(isTextAreaFilled());
	}

	private boolean isTextAreaFilled() {
		return !view.getEnteredText().trim().isEmpty();
	}

	@Override
	public void sendEmoticonButtonClicked() {
		view.showEmoticonPopup();
	}
}

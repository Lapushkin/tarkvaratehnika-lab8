package ee.ttu.tarkvaratehnika.chat.client.ui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.BadLocationException;

public class ChatWindow extends JFrame implements ChatWindowView {
	private static final long serialVersionUID = 1L;

	protected JTextArea chatContents;
	protected JTextField textEntryField;
	protected JButton sendButton;
	public JButton emoticonButton;
	public EmoticonPopup emoticonWindow;

	protected ChatWindowPresenter presenter;

	public ChatWindow() {
		setSize(800, 600);
		setLocation(100, 100);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		setLayout(new BorderLayout());
		Container contentPane = getContentPane();

		contentPane.add(createChatContents(), BorderLayout.CENTER);
		contentPane.add(createTextEntryArea(), BorderLayout.SOUTH);

		initEmoticonPopup();

		textEntryField.grabFocus();

		presenter = new ChatWindowPresenter(this);
	}

	private void initEmoticonPopup() {
		emoticonWindow = new EmoticonPopup(this, "Choose emoticon", true, textEntryField);
		emoticonWindow.setLocation(150,150);
		emoticonWindow.setSize(50, 150);
	}

	private JComponent createChatContents() {
		chatContents = new JTextArea();
		chatContents.setEditable(false);
		chatContents.setLineWrap(true);

		JScrollPane scrollPane = new JScrollPane(chatContents);
		scrollPane.setAutoscrolls(true);
		scrollPane
				.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		return scrollPane;
	}

	private JComponent createTextEntryArea() {
		JPanel result = new JPanel();
		result.setLayout(new BorderLayout());
		result.add(createTextEntryField(), BorderLayout.CENTER);
		JPanel buttons = new JPanel();
		buttons.add(createSendButton());
		buttons.add(createEmoticonButton());
		result.add(buttons, BorderLayout.EAST);
		return result;
	}

	private JButton createEmoticonButton() {
		emoticonButton = new JButton("Emoticons");
		emoticonButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				presenter.sendEmoticonButtonClicked();
			}
		});
		return emoticonButton;
	}

	private JTextField createTextEntryField() {
		textEntryField = new JTextField();
		textEntryField.getDocument().addDocumentListener(
				new DocumentListener() {
					public void changedUpdate(DocumentEvent e) {
						presenter.enteredTextChanged();
					}

					public void removeUpdate(DocumentEvent e) {
						presenter.enteredTextChanged();
					}

					public void insertUpdate(DocumentEvent e) {
						presenter.enteredTextChanged();
					}
				});
		textEntryField.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				presenter.sendButtonClicked();
			}
		});
		return textEntryField;
	}

	private JButton createSendButton() {
		sendButton = new JButton("SEND");
		sendButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				presenter.sendButtonClicked();
			}
		});
		return sendButton;
	}

	@Override
	public void setChatAreaContents(String chat) {
		chatContents.setText(chat);
		chatContents.setCaretPosition(chatContents.getDocument().getLength());
	}

	@Override
	public String getEnteredText() {
		return textEntryField.getText();
	}

	@Override
	public void setEnteredText(String text) {
		textEntryField.setText(text);
		textEntryField.grabFocus();
	}

	public ChatWindowPresenter getPresenter() {
		return presenter;
	}

	@Override
	public void setSendButtonEnabled(boolean enabled) {
		sendButton.setEnabled(enabled);
	}

	@Override
	public void showEmoticonPopup() {
		emoticonWindow.setVisible(true);
	}
}

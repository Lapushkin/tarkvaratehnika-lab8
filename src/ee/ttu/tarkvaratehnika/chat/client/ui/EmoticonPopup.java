package ee.ttu.tarkvaratehnika.chat.client.ui;

import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import javax.swing.text.BadLocationException;
import java.awt.event.MouseEvent;

/**
 * Created by Alexander on 29.10.15.
 */
public class EmoticonPopup extends JDialog {

    private JTable emoticonsTable;
    private JTextField textField;

    public EmoticonPopup(ChatWindow chatWindow, String s, boolean b, JTextField textField) {
        super(chatWindow, s, b);
        this.textField = textField;
        Object[][] array = new Object[2][3];
        array[0][0] = ":)";
        array[0][1] = ":(";
        array[0][2] = ":S";
        array[1][0] = ":P";
        array[1][1] = ";(";
        array[1][2] = ":D";
        this.emoticonsTable = new JTable(array, new Object[]{"col1", "col2", "col3"});
        emoticonsTable.setRowHeight(40);
        emoticonsTable.changeSelection(0,0,false, false);
        emoticonsTable.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                insertEmoticonAtCaret((String)emoticonsTable.getModel().getValueAt(emoticonsTable.getSelectedRow(), emoticonsTable.getSelectedColumn()));
                chatWindow.emoticonWindow.setVisible(false);
            }
        });
        this.add(emoticonsTable);
    }

    private void insertEmoticonAtCaret(String valueAt) {
        textField.setText(textField.getText() + valueAt);
    }

    public JTable getEmoticonsTable() {
        return emoticonsTable;
    }
}

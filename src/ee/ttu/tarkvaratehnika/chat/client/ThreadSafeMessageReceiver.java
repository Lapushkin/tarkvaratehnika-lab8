package ee.ttu.tarkvaratehnika.chat.client;

import javax.swing.SwingUtilities;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ThreadSafeMessageReceiver implements MessageReceiver {
	private final MessageReceiver delegate;
	
	public ThreadSafeMessageReceiver(MessageReceiver delegate) {
		this.delegate = delegate;
	}

	@Override
	public void connectedToServer(final MessageSender connection) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				delegate.connectedToServer(connection);
			}
		});
	}

	@Override
	public void messageFromServer(final MessageSender connection, final Message message) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				delegate.messageFromServer(connection, message);
			}
		});
	}

	@Override
	public void sendEmoticonButtonClicked() {
		// TODO Auto-generated method stub
		
	}
}

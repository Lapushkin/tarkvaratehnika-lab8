package ee.ttu.tarkvaratehnika.chat.client;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import ee.ttu.tarkvaratehnika.chat.server.Server;
import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class Connection implements MessageSender {
	private final MessageReceiver messageReceiver;
	private final Socket socket;
	private final ObjectInputStream in;
	private final ObjectOutputStream out;

	public Connection(MessageReceiver messageReceiver) throws IOException {
		this(messageReceiver, Server.DEFAULT_SERVER_PORT);
	}

	public Connection(MessageReceiver messageReceiver, int port)
			throws IOException {
		this.messageReceiver = messageReceiver;

		this.socket = new Socket("localhost", port);
		this.in = new ObjectInputStream(socket.getInputStream());
		this.out = new ObjectOutputStream(socket.getOutputStream());

		new Thread() {
			@Override
			public void run() {
				readMessagesFromServer();
			}
		}.start();
	}

	private void readMessagesFromServer() {
		messageReceiver.connectedToServer(this);
		try {
			while (true) {
				Message message = (Message) in.readObject();
				messageReceiver.messageFromServer(this, message);
			}
		} catch (EOFException e) {
			return;
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	};

	@Override
	public void sendToServer(Message message) {
		try {
			out.writeObject(message);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void close() throws IOException {
		socket.close();
	}
}

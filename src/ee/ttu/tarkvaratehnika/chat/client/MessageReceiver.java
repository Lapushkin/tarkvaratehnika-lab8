package ee.ttu.tarkvaratehnika.chat.client;

import ee.ttu.tarkvaratehnika.chat.shared.Message;


public interface MessageReceiver {
	public void connectedToServer(MessageSender connection);
	
	public void messageFromServer(MessageSender connection, Message message);

	public void sendEmoticonButtonClicked();

}

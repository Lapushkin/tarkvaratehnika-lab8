package ee.ttu.tarkvaratehnika.chat;

import ee.ttu.tarkvaratehnika.chat.client.Connection;
import ee.ttu.tarkvaratehnika.chat.client.ui.ChatWindow;

public class ClientLauncher {
	public static void main(String[] args) throws Exception {
		ChatWindow chatWindow = new ChatWindow();
		new Connection(chatWindow.getPresenter(), ServerLauncher.DEFAULT_PORT);
		chatWindow.setVisible(true);
	}
}

package ee.ttu.tarkvaratehnika.chat.server;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class MessageProcessor {
	private static final String LIST_USERS_COMMAND = "/listusers";

	private List<ClientConnection> connectedClients = new ArrayList<ClientConnection>();
	private Map<ClientConnection, String> usernames = new LinkedHashMap<ClientConnection, String>();

	protected void messageReceivedFrom(ClientConnection client, Message message) {
		if (LIST_USERS_COMMAND.equalsIgnoreCase(message.getText())) {
			client.send(new Message(LIST_USERS_COMMAND));
			listUsersTo(client);
		} else {
			sendToEveryone(new Message(usernames.get(client) + ": "
					+ message.getText()));
		}
	}

	private void listUsersTo(ClientConnection client) {
		synchronized (connectedClients) {
			for (ClientConnection aClient : connectedClients) {
				client.send(new Message(usernames.get(aClient)));
			}
		}
	}

	private void sendToEveryone(Message message) {
		synchronized (connectedClients) {
			for (ClientConnection aClient : connectedClients) {
				aClient.send(message);
			}
		}
	}

	public void clientConnected(ClientConnection client) {
		synchronized (connectedClients) {
			connectedClients.add(client);
			usernames.put(client, generateNewUsername());
		}
	}

	private int userCount = 0;

	private String generateNewUsername() {
		return "user" + (++userCount);
	}

	public void clientDisconnected(ClientConnection client) {
		synchronized (connectedClients) {
			connectedClients.remove(client);
			usernames.remove(client);
		}
	}
}

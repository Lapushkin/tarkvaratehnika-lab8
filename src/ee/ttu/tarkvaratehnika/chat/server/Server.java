package ee.ttu.tarkvaratehnika.chat.server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class Server implements Runnable {
	public static final int DEFAULT_SERVER_PORT = 8000;
	
	private final int port;
	private final MessageProcessor messageProcessor;
	private ServerSocket socket;
	
	public Server(MessageProcessor messageProcessor) {
		this(messageProcessor, DEFAULT_SERVER_PORT);
	}
	
	public Server(MessageProcessor messageProcessor, int port) {
		this.messageProcessor = messageProcessor;
		this.port = port;
	}

	public void run() {
		waitForConnections();
	}

	private void waitForConnections() {
		try {
			socket = new ServerSocket(port);
			while(true) {
	 			Socket connection = socket.accept();
				acceptNewClient(connection);
			}
		} catch (Exception e) {
			if(isSocketClosedException(e)) {
				return;
			}
			throw new RuntimeException(e);
		} finally  {
			close(socket);
		}
	}

	protected void acceptNewClient(Socket connection) throws IOException {
		ObjectOutputStream out = new ObjectOutputStream(connection.getOutputStream());
		ObjectInputStream in = new ObjectInputStream(connection.getInputStream());
		ClientConnection clientConnection = new ClientConnection(in, out, messageProcessor);
		new Thread(clientConnection).start();
	}
	
	private boolean isSocketClosedException(Exception e) {
		return e instanceof SocketException && 
				("socket closed".equals(e.getMessage())) || "Socket is closed".equals(e.getMessage());
	}
	
	public void stop() {
		close(socket);
	}

	private void close(ServerSocket closeable) {
		if(closeable != null) {
			try {
				closeable.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
}

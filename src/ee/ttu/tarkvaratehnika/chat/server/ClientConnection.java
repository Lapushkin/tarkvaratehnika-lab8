package ee.ttu.tarkvaratehnika.chat.server;

import java.io.EOFException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import ee.ttu.tarkvaratehnika.chat.shared.Message;

public class ClientConnection implements Runnable {
	private ObjectInputStream in;
	private ObjectOutputStream out;
	private final MessageProcessor messageProcessor;

	public ClientConnection(ObjectInputStream in, ObjectOutputStream out, MessageProcessor messageProcessor) {
		this.in = in;
		this.out = out;
		this.messageProcessor = messageProcessor;
	}

	@Override
	public void run() {
		messageProcessor.clientConnected(this);
		try {
			readInputMessages();
		} finally {
			messageProcessor.clientDisconnected(this);
		}
	}

	private void readInputMessages() {
		try {
			while(true) {
				Message message = (Message)in.readObject();
				messageProcessor.messageReceivedFrom(this, message);
			}
		} catch(EOFException e) {
			return;
		} catch(Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void send(Message message) {
		try {
			out.writeObject(message);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}

package ee.ttu.tarkvaratehnika.chat.shared;

import java.io.Serializable;

public class Message implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final String content;

	public Message(String content) {
		this.content = content;
	}

	public String getText() {
		return content;
	}
}

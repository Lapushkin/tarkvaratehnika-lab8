package ee.ttu.tarkvaratehnika.chat;

import ee.ttu.tarkvaratehnika.chat.server.MessageProcessor;
import ee.ttu.tarkvaratehnika.chat.server.Server;

public class ServerLauncher {
	public static final int DEFAULT_PORT = 8111;

	public static void main(String[] args) throws Exception {
		Server server = new Server(new MessageProcessor(), DEFAULT_PORT);

		System.out.println("Server running");
		server.run();
	}
}
